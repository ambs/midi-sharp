# MidiSharp

This small C# class is a simple rewrite of Kevin Boone wonderful
Java code available at http://kevinboone.me/javamidi.html.

The current version of this repository is under work. I expect to
document it a little, and probably prepare a NuGet package in the future.
